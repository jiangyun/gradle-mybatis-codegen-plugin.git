package ${basePackage}.${mapper}

import org.apache.ibatis.annotations.Param
import org.springframework.stereotype.Repository
<#if haveBigDecimal>
import java.math.BigDecimal
</#if>
<#if haveDate>
import java.util.Date
</#if>
import java.util.List

import ${basePackage}.${entity}.${className}Entity
import ${basePackage}.vo.${className}VO

/**
 * @描述: ${comment}
 * @作者: ${author}
 * @创建时间: ${createTime}
 * @所属公司: ${company}
 */
@Repository
interface ${className}Mapper {

    /**
	 * 查询列表
	 *
	 * @param pojo 查询过滤参数
	 * @return
	 **/
    fun select(@Param(value = "pojo") pojo: ${className}VO): MutableList${r'<'}${className}VO>?

    /**
     * 查询列表(仅查询单表字段)
     *
     * @param pojo 查询过滤参数
     * @return
     **/
     fun selectOnlySelf(@Param(value = "pojo") pojo: ${className}VO): MutableList${r'<'}${className}VO>?

    /**
     * 根据ID查询数据
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @return
     **/
    fun selectById(<#list pkColumns as column>@Param(value = "${column.attrName}") ${column.attrName}: ${column.attrType}<#if column_has_next>, </#if></#list>): ${className}VO?

    /**
     * 新增数据
     *
     * @param pojo 待新增数据
     * @return
     **/
    fun insert(@Param(value = "pojo") pojo: ${className}Entity): Int

    /**
     * 批量新增
     *
     * @param pojos 待新增数据集合
     * @return
     **/
    fun insertBatch(@Param(value = "pojos") pojos: MutableList${r'<'}${className}Entity>): Int

    /**
     * 更新数据
     *
     * @param pojo 待更新数据
     * @return
     **/
    fun update(@Param(value = "pojo") pojo: ${className}Entity): Int

    /**
     * 逻辑删除
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @param actionUserId 操作用户ID
     * @return
     **/
    fun delete(<#list pkColumns as column>@Param(value = "${column.attrName}") ${column.attrName}: ${column.attrType}, </#list> @Param(value = "actionUserId") String actionUserId)

    /**
     * 物理删除
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @return
     **/
     fun deleteByPk(<#list pkColumns as column>@Param(value = "${column.attrName}") ${column.attrName}: ${column.attrType}<#if column_has_next>, </#if></#list>)

}