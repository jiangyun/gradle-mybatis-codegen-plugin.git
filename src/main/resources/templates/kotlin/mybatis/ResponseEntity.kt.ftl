package ${basePackage}.common

<#if enableGenerateDoc>
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
</#if>

/**
 * @描述: 响应结果
 * @作者: ${author}
 * @创建时间: ${createTime}
 * @所属公司: ${company}
 */
<#if enableGenerateDoc>
@ApiModel(value = "响应结果")
</#if>
data class ResponseEntity${r'<'}T>(
        <#if enableGenerateDoc>@ApiModelProperty(value = "返回码", dataType = "string", example = "000000")</#if>
        var code: String? = null, // 返回码
        <#if enableGenerateDoc>@ApiModelProperty(value = "数据", dataType = "string", example = "{\"userName\": \"iwiteks\", \"password\": \"admin123456\"}")</#if>
        var data: T? = null, // 数据
        <#if enableGenerateDoc>@ApiModelProperty(value = "返回信息", dataType = "string", example = "获取数据成功")</#if>
        var message: String? = null, // 返回信息
        <#if enableGenerateDoc>@ApiModelProperty(value = "数据长度", dataType = "long", example = "1024")</#if>
        var total: Long? = null // 数据长度
) {
    companion object {
        const val CODE_SUCCESS: String = "000000"
        const val CODE_FAILURE: String = "100000"
        const val PARAM_ERROR_CODE: String = "100001"
        const val CODE_ERROR: String = "200000"
        const val MSG_SUCCESS: String = "成功"
        const val MSG_FAILURE: String = "失败"
        const val MSG_ERROR: String = "错误"

        fun ${r'<'}T> success(): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = CODE_SUCCESS,
                message = MSG_SUCCESS
            )
        }

        fun ${r'<'}T> success(msg: String?): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = CODE_SUCCESS,
                message = if (!msg.isNullOrBlank()) msg else MSG_SUCCESS
            )
        }

        fun ${r'<'}T> success(data: T?): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = CODE_SUCCESS,
                data = data,
                message = MSG_SUCCESS
            )
        }

        fun ${r'<'}T> success(data: T?, total: Long?): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = CODE_SUCCESS,
                data = data,
                message = MSG_SUCCESS,
                total = total
            )
        }

        fun ${r'<'}T> success(data: T?, msg: String?): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = CODE_SUCCESS,
                data = data,
                message = if (msg.isBlank()) MSG_SUCCESS else msg,
                total = total
            )
        }

        fun ${r'<'}T> success(data: T?, msg: String?, total: Long?): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = CODE_SUCCESS,
                data = data,
                message = if (!msg.isNullOrBlank()) msg else MSG_SUCCESS,
                total = total
            )
        }

        fun ${r'<'}T> failure(): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = CODE_FAILURE,
                message = MSG_FAILURE
            )
        }

        fun ${r'<'}T> failure(msg: String?): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = CODE_FAILURE,
                message = if (!msg.isNullOrBlank()) msg else MSG_FAILURE
            )
        }

        fun ${r'<'}T> failure(code: String?, msg: String?): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = if (!code.isNullOrBlank()) code else CODE_FAILURE,
                message = if (!msg.isNullOrBlank()) msg else MSG_FAILURE
            )
        }

        fun ${r'<'}T> error(): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = CODE_ERROR,
                message = MSG_ERROR
            )
        }

        fun ${r'<'}T> error(msg: String?): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = CODE_ERROR,
                message = if (!msg.isNullOrBlank()) msg else MSG_ERROR
            )
        }

        fun ${r'<'}T> error(code: String?, msg: String?): ResponseEntity${r'<'}T> {
            return ResponseEntity(
                code = if (!code.isNullOrBlank()) code else CODE_ERROR,
                message = if (!msg.isNullOrBlank()) msg else MSG_ERROR
            )
        }
    }
}