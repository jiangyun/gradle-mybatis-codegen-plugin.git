package ${basePackage}.${service}

import org.springframework.stereotype.Service

import com.github.pagehelper.PageHelper
import com.github.pagehelper.PageInfo

import ${basePackage}.${entity}.${className}Entity
import ${basePackage}.vo.${className}VO
import ${basePackage}.pojo.${className}POJO
import ${basePackage}.${mapper}.${className}Mapper
import ${basePackage}.utils.GenerateCodeUtils

/**
 * ${comment}
 * @date ${createTime}
 **/
@Service
class ${className}Service(val ${className?uncap_first}Mapper: ${className}Mapper) {

    /**
      * 查询列表
      *
      * @param param 查询过滤参数
      * @return
      **/
     fun getList(pageNum: Int, pageSize: Int, ${className}VO param): MutableList${r'<'}${className}VO> {
         PageHelper.startPage${r'<'}${className}VO>(pageNum, pageSize)
         var results: MutableList${r'<'}${className}VO>? = ${className?uncap_first}Mapper.select(param)

         return PageInfo${r'<'}${className}VO>(results)
     }

     /**
      * 根据ID查询数据
      *
      <#list pkColumns as column>
      * @param ${column.attrName} ${column.columnComment}
      </#list>
      * @return
      **/
     fun getById(<#list pkColumns as column>${column.attrName}: ${column.attrType}<#if column_has_next>, </#if></#list>): ${className}VO {
         var result: ${className}VO = ${className?uncap_first}Mapper.selectById(<#list pkColumns as column>${column.attrName}<#if column_has_next>, </#if></#list>)

         return result
     }

     /**
      * 新增数据
      *
      * @param data         待新增数据
      * @param actionUserId  操作用户ID
      * @return
      **/
     fun add(data: ${className}POJO, String actionUserId: String): Boolean {
        var param: ${className}Entity = ${className}Entity()
         <#list columns as column>
            <#if (pkColumns?size == 1 && column.columnKey == "PRI")>
         param.${column.attrName} = GenerateCodeUtils.generateId()
            <#elseif (column.attrName == "createBy" || column.attrName =="createUser" || column.attrName == "updateBy" || column.attrName == "updateUser")>
         param.${column.attrName} = actionUserId
            <#elseif (column.attrName == "createAt" || column.attrName == "createTime" || column.attrName == "createDate" || column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate")>
         param.${column.attrName} = Date()
            <#elseif (column.attrName == "valid")>
         param.${column.attrName} = 1
            <#else>
         param.${column.attrName} = data.${column.attrName}
            </#if>
         </#list>

         return ${className?uncap_first}Mapper.insert(param) > 0
     }

     /**
      * 批量新增
      *
      * @param datas        待新增数据集合
      * @param actionUserId  操作用户ID
      * @return
      **/
     fun addBatch(datas: MutableList${r'<'}${className}POJO>, actionUserId: String): Boolean {
         var params: MutableList${r'<'}${className}Entity> = mutableListOf()
         var param: ${className}Entity?
         for(${className}POJO data : datas) {
            param = ${className}Entity()
            <#list columns as column>
                <#if (pkColumns?size == 1 && column.columnKey == "PRI")>
            param.${column.attrName} = GenerateCodeUtils.generateId()
                <#elseif (column.attrName == "createBy" || column.attrName =="createUser" || column.attrName == "updateBy" || column.attrName == "updateUser")>
            param.${column.attrName} = actionUserId
                <#elseif (column.attrName == "createAt" || column.attrName == "createTime" || column.attrName == "createDate" || column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate")>
            param.${column.attrName} = Date()
                <#elseif (column.attrName == "valid")>
            param.${column.attrName} = 1
                <#else>
            param.${column.attrName} = data.${column.attrName}
                </#if>
            </#list>

            params.add(param)
         }

         return ${className?uncap_first}Mapper.insertBatch(params) > 0
     }

     /**
      * 更新数据
      *
      * @param data          待更新数据
      * @param actionUserId  操作用户ID
      * @return
      **/
     fun modify(data: ${className}POJO, actionUserId: String): Boolean {
         var param: ${className}Entity = ${className}Entity()
         <#list columns as column>
            <#if (column.attrName == "updateBy" || column.attrName == "updateUser")>
         param.${column.attrName} = actionUserId
            <#elseif (column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate")>
         param.${column.attrName} = Date()
            <#elseif (column.attrName == "valid")>
         param.${column.attrName} = 1
            <#else>
         param.${column.attrName} = data.${column.attrName}
            </#if>
         </#list>

         return ${className?uncap_first}Mapper.update(param) > 0
     }

     /**
      * 逻辑删除
      *
      <#list pkColumns as column>
      * @param ${column.attrName} ${column.columnComment}
      </#list>
      * @param actionUserId 操作用户ID
      * @return
      **/
     fun delete(<#list pkColumns as column>${column.attrName}: ${column.attrType}, </#list>actionUserId: String): Boolean {
         return ${className?uncap_first}Mapper.delete(<#list pkColumns as column>${column.attrName}, </#list> actionUserId) > 0
     }
}