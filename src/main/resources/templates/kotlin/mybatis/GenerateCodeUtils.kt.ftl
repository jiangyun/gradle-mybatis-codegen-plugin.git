package ${basePackage}.utils

import cn.hutool.core.date.DateUtil
import cn.hutool.core.lang.Snowflake
import cn.hutool.core.util.IdUtil
import org.apache.commons.lang3.RandomStringUtils

import java.util.Date
import java.util.Random

/**
* @描述: 生成ID及定长数据工具类
* @作者: ${author}
* @创建时间: ${createTime}
* @所属公司: ${company}
*/
object GenerateCodeUtils {

    /**
     * 生成UUID
     *
     * @return
     */
    @Synchronized
    fun generateId(): String? {
        return IdUtil.fastSimpleUUID()
    }

    /**
     * 生成微信订单号
     *
     * @return
     */
    @Synchronized
    fun genWechatPayOutTradeNo(): String? {
        return IdUtil.fastSimpleUUID().substring(0, 28)
    }

    /**
     * 雪花算法生成ID
     *
     * @return
     */
    @Synchronized
    fun snowflakeId(): String? {
        val workerId = 1L //为终端ID
        val dataCenterId = 1L //数据中心ID
        val snowflake = IdUtil.createSnowflake(workerId, dataCenterId)
        return snowflake.nextId().toString()
    }

    /**
     * 雪花算法生成ID
     *
     * @param workerId     终端ID
     * @param dataCenterId 数据中心ID
     * @return
     */
    @Synchronized
    fun snowflakeId(workerId: Long, dataCenterId: Long): String? {
        val snowflake = IdUtil.createSnowflake(workerId, dataCenterId)
        return snowflake.nextId().toString()
    }

    /**
     * 生成固长字符串
     *
     * @return
     */
    fun generateAppKey(): String? { //length表示生成字符串的长度
        val base = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM-1234567890"
        val random = Random()
        val sb = StringBuffer()
        for (i in 0..47) {
            val number = random.nextInt(base.length)
            sb.append(base[number])
        }
        return sb.toString()
    }

    /**
     * 生成固长字符串
     *
     * @param length
     * @return
     */
    fun getRandomString(length: Int): String? { //length表示生成字符串的长度
        val base = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM-1234567890"
        val random = Random()
        val sb = StringBuffer()
        for (i in 0 until length) {
            val number = random.nextInt(base.length)
            sb.append(base[number])
        }
        return sb.toString()
    }

    /**
     * 生成固长数字
     *
     * @param length
     * @return
     */
    fun getRandomLong(length: Int): String? { //length表示生成字符串的长度
        val base = "1234567891234567891234567891234566789"
        val random = Random()
        val sb = StringBuffer()
        for (i in 0 until length) {
            val number = random.nextInt(base.length)
            sb.append(base[number])
        }
        return sb.toString()
    }

    /**
     * 生成固长数字
     *
     * @return
     */
    fun getRandomLongId(): String? {
        val base = "1234567891234567891234567891234566789"
        val random = Random()
        val sb = StringBuffer()
        for (i in 0..19) {
            val number = random.nextInt(base.length)
            sb.append(base[number])
        }
        return sb.toString()
    }

    /**
     * 生成固长字符串
     *
     * @param appKey
     * @return
     */
    fun generateAppSecret(appKey: String): String? {
        val base = "9qw\$e678rtyu-iopa45sdfghjklzx0c@vbnmQW-ERTY12UIO-PAS3DFGH-JKLZXC@VBNM$appKey"
        val random = Random()
        val sb = StringBuffer()
        for (i in 0..63) {
            val number = random.nextInt(base.length)
            sb.append(base[number])
        }
        return sb.toString()
    }

    /**
     * 生成用户密码加密盐值
     *
     * @return
     */
    fun generateSalt(): String? { //length表示生成字符串的长度
        val base = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM-1234567890"
        val random = Random()
        val sb = StringBuffer()
        for (i in 0..31) {
            val number = random.nextInt(base.length)
            sb.append(base[number])
        }
        return sb.toString()
    }

    /**
     * 生成订单编号,当前时间 + 手机尾号 + 6位随机数
     *
     * @param mobile 手机号
     * @return
     */
    @Synchronized
    fun generateOutTradeNo(mobile: String): String? {
        val time = DateUtil.format(Date(), "yyyyMMddHHmmss")
        val randomNum = RandomStringUtils.random(6, "0123456789")
        val mobileSuffix = mobile.substring(mobile.length - 4)
        return time + mobileSuffix + randomNum
    }
}