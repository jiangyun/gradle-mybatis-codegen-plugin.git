package ${basePackage}.vo;

<#if enableGenerateDoc>
import io.swagger.v3.oas.annotations.media.Schema;
</#if>
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveLocalDate>
import java.time.LocalDate;
</#if>
<#if haveLocalDateTime>
import java.time.LocalDateTime;
</#if>
<#if haveLocalTime>
import java.time.LocalTime;
</#if>

/**
 * ${comment}
 *
 * @author ${author}
 * @since ${createTime}
 */
<#if enableGenerateDoc>
@Schema(description = "${comment}返回信息")
</#if>
@Data
@Accessors(chain = true)
public class ${className}VO implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    <#list columns as column>
    <#if (column.attrName == "deleted")>
    <#else>
    /**
     * ${column.columnComment}
     */
    <#if enableGenerateDoc>
    @Schema(description = "${column.columnComment}", example = "${column.example}")
    </#if>
    private ${column.attrType} ${column.attrName};
    </#if>
</#list>
}
