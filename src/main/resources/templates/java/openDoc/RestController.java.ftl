package ${basePackage}.${controller};

<#if enableGenerateDoc>
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
</#if>
import com.uniplore.core.common.web.Result;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import java.util.Collections;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>

import ${basePackage}.vo.${className}VO;
import ${basePackage}.dto.${className}AddDTO;
import ${basePackage}.dto.${className}UpdateDTO;
import ${basePackage}.${service}.${className}Service;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;

/**
 * ${comment}接口
 *
 * @author ${author}
 * @since ${createTime}
 */
<#if enableGenerateDoc>
@Tag(name = "${comment}接口", description = "${comment}接口")
</#if>
@Validated
@RestController
@RequestMapping(value = "${apiPrefix}/${className?uncap_first}")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ${className}Controller {

    private final ${className}Service ${className?uncap_first}Service;

    /**
	 * 分页获取列表
	 *
	 * @param current     当前页
	 * @param size        每页大小
	 * @param param       其他查询参数
	 * @return {@link Result}${r'<{@link List}<'}{@link ${className}VO}>>}
	 **/
    <#if enableGenerateDoc>
    @Operation(summary = "分页获取列表", description = "分页获取列表")
    @Parameters({
            @Parameter(name = "current", description = "当前页", example = "1", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "size", description = "每页大小", example = "10", required = true, in = ParameterIn.QUERY),
    })
    </#if>
    @GetMapping(value = "/pageList")
    public Result${r'<List<'}${className}VO>> pageList(
            @Min(value = 1) @RequestParam(required = true) int current,
            @RequestParam(required = true) int size,
            @Valid ${className}VO param
    ) {
        IPage${r'<'}${className}VO> pageInfo = ${className?uncap_first}Service.getPage(new Page<>(current, size), param);
        if (pageInfo == null) {
            return Result.pageResult(Collections.emptyList(), 0L);
        }

        return Result.pageResult(pageInfo.getRecords(), pageInfo.getTotal());
    }

    /**
     * 查询一条数据
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @return {@link Result}${r'<{@link '}${className}VO}>
     */
    <#if enableGenerateDoc>
    @Operation(summary = "查询一条数据", description = "查询一条数据")
    @Parameters({
        <#list pkColumns as column>
        @Parameter(name = "${column.attrName}", description = "${column.columnComment}", example = "123456", required = true, in = ParameterIn.PATH)<#if column_has_next>,</#if>
        </#list>
    })
    </#if>
    @GetMapping(value = "/getOne<#list pkColumns as column>/{${column.attrName}}</#list>")
    public Result${r'<'}${className}VO> getOne(<#list pkColumns as column>@PathVariable(value = "${column.attrName}") ${column.attrType} ${column.attrName}<#if column_has_next>, </#if></#list>) {
        ${className}VO result = ${className?uncap_first}Service.getOne(<#list pkColumns as column>${column.attrName}<#if column_has_next>, </#if></#list>);

        return Result.success(result);
    }

    /**
     * 新增数据
     *
     * @param data    数据
     * @return {@link Result${r'<'}Boolean>}
     **/
    <#if enableGenerateDoc>
    @Operation(summary = "新增数据", description = "新增数据")
    </#if>
    @PostMapping(value = "/save")
    public Result${r'<'}Boolean> save(@Valid @RequestBody ${className}AddDTO data) {
        boolean result = ${className?uncap_first}Service.add(data);

        return result ? Result.success(result) : Result.failure();
    }

    /**
     * 更新数据
     *
     * @param data     数据
     * @return {@link Result${r'<'}Boolean>}
     **/
    <#if enableGenerateDoc>
    @Operation(summary = "更新数据", description = "更新数据")
    @Parameters({
         <#list pkColumns as column>
         @Parameter(name = "${column.attrName}", description = "${column.columnComment}", example = "123456", required = true, in = ParameterIn.PATH)<#if column_has_next>,</#if>
         </#list>
     })
    </#if>
    @PutMapping(value = "/modify<#list pkColumns as column>/{${column.attrName}}</#list>")
    public Result${r'<'}Boolean> modify(<#list pkColumns as column>@PathVariable(value = "${column.attrName}") ${column.attrType} ${column.attrName}, </#list>@Valid @RequestBody ${className}UpdateDTO data) {
        <#list pkColumns as column>
        data.set${column.upcaseAttrName}(${column.attrName});
        </#list>
        boolean result = ${className?uncap_first}Service.modify(data);

        return result ? Result.success(result) : Result.failure();
    }

   /**
    * 删除一条数据
    *
    <#list pkColumns as column>
    * @param ${column.attrName} ${column.columnComment}
    </#list>
    * @return {@link Result${r'<'}Boolean>}
    **/
    <#if enableGenerateDoc>
    @Operation(summary = "删除一条数据", description = "删除一条数据")
    @Parameters({
        <#list pkColumns as column>
        @Parameter(name = "${column.attrName}", description = "${column.columnComment}", example = "123456", required = true, in = ParameterIn.PATH)<#if column_has_next>,</#if>
        </#list>
    })
    </#if>
    @DeleteMapping(value = "/delete<#list pkColumns as column>/{${column.attrName}}</#list>")
    public Result${r'<'}Boolean> delete(<#list pkColumns as column>@PathVariable(value = "${column.attrName}") ${column.attrType} ${column.attrName}<#if column_has_next>, </#if></#list>) {
        boolean result = ${className?uncap_first}Service.removeOne(<#list pkColumns as column>${column.attrName}<#if column_has_next>, </#if></#list>);

        return result ? Result.success(result) : Result.failure();
    }

}