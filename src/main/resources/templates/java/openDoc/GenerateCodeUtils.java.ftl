package ${basePackage}.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Date;
import java.util.Random;

/**
 * 生成ID及定长数据工具类
 *
 * @author ${author}
 * @since ${createTime}
 */
public class GenerateCodeUtils {

    /**
     * 生成UUID
     *
     * @return
     */
    public static synchronized String generateId() {
        return IdUtil.fastSimpleUUID();
    }

    /**
     * 生成微信订单号
     *
     * @return
     */
    public static synchronized String genWechatPayOutTradeNo() {
        return IdUtil.fastSimpleUUID().substring(0, 28);
    }

    /**
     * 雪花算法生成ID
     *
     * @return
     */
    public static synchronized String snowflakeId() {
        long workerId = 1L;//为终端ID
        long dataCenterId = 1L;//数据中心ID
        Snowflake snowflake = IdUtil.createSnowflake(workerId, dataCenterId);

        return String.valueOf(snowflake.nextId());
    }

    /**
     * 雪花算法生成ID
     *
     * @param workerId     终端ID
     * @param dataCenterId 数据中心ID
     * @return
     */
    public static synchronized String snowflakeId(long workerId, long dataCenterId) {
        Snowflake snowflake = IdUtil.createSnowflake(workerId, dataCenterId);

        return String.valueOf(snowflake.nextId());
    }

    /**
     * 生成固长字符串
     *
     * @return
     */
    public static String generateAppKey() { //length表示生成字符串的长度
        String base = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM-1234567890";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 48; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 生成固长字符串
     *
     * @param length
     * @return
     */
    public static String getRandomString(int length) { //length表示生成字符串的长度
        String base = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM-1234567890";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 生成固长数字
     *
     * @param length
     * @return
     */
    public static String getRandomLong(int length) { //length表示生成字符串的长度
        String base = "1234567891234567891234567891234566789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 生成固长数字
     *
     * @return
     */
    public static String getRandomLongId() {
        String base = "1234567891234567891234567891234566789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 20; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 生成固长字符串
     *
     * @param appKey
     * @return
     */
    public static String generateAppSecret(String appKey) {
        String base = "9qw$e678rtyu-iopa45sdfghjklzx0c@vbnmQW-ERTY12UIO-PAS3DFGH-JKLZXC@VBNM" + appKey;
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 64; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 生成用户密码加密盐值
     *
     * @return
     */
    public static String generateSalt() { //length表示生成字符串的长度
        String base = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM-1234567890";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 32; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 生成订单编号,当前时间 + 手机尾号 + 6位随机数
     *
     * @param mobile 手机号
     * @return
     */
    public synchronized static String generateOutTradeNo(String mobile) {
        String time = DateUtil.format(new Date(), "yyyyMMddHHmmss");
        String randomNum = RandomStringUtils.random(6, "0123456789");
        String mobileSuffix = mobile.substring(mobile.length() - 4);
        return time + mobileSuffix + randomNum;
    }
}
