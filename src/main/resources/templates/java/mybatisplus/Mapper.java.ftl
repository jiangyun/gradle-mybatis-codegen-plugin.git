package ${basePackage}.${mapper};

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveLocalDate>
import java.time.LocalDate;
</#if>
<#if haveLocalDateTime>
import java.time.LocalDateTime;
</#if>
<#if haveLocalTime>
import java.time.LocalTime;
</#if>

import ${basePackage}.${entity}.${className}Entity;
import ${basePackage}.vo.${className}VO;

/**
 * ${comment}Mapper
 *
 * @author ${author}
 * @since ${createTime}
 */
@Repository
public interface ${className}Mapper extends BaseMapper${r'<'}${className}Entity> {

    /**
     * 分页查询列表
     *
     * @param pageInfo   分页插件
     * @param param      查询参数
     * @return {@link IPage<${className}VO>}
     **/
    IPage${r'<'}${className}VO> findPage(
        IPage${r'<'}${className}VO> pageInfo,
        @Param(value = "param") ${className}VO param
    );

    /**
     * 查询一条数据
     *
    <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
    </#list>
     * @return {@link ${className}VO}
     **/
    ${className}VO findByPrimaryKey(<#list pkColumns as column>@Param(value = "${column.attrName}") ${column.attrType} ${column.attrName}<#if column_has_next>, </#if></#list>);
}