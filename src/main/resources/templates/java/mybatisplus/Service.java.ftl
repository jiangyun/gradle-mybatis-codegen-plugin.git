package ${basePackage}.${service};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.ArrayList;
import lombok.RequiredArgsConstructor;

<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveLocalDate>
import java.time.LocalDate;
</#if>
<#if haveLocalDateTime>
import java.time.LocalDateTime;
</#if>
<#if haveLocalTime>
import java.time.LocalTime;
</#if>

import ${basePackage}.${entity}.${className}Entity;
import ${basePackage}.vo.${className}VO;
import ${basePackage}.dto.${className}AddDTO;
import ${basePackage}.dto.${className}UpdateDTO;
import ${basePackage}.${mapper}.${className}Mapper;

/**
 * ${comment}
 *
 * @author ${author}
 * @since ${createTime}
 */
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ${className}Service extends ServiceImpl${r'<'}${className}Mapper, ${className}Entity> implements IService${r'<'}${className}Entity> {

    private final ${className}Mapper ${className?uncap_first}Mapper;


    /**
     * 分页查询
     *
     * @param pageInfo   分页插件
     * @param param      查询参数
     * @return {@link IPage${r'<'}${className}VO>}
     */
    public IPage${r'<'}${className}VO> getPage(IPage${r'<'}${className}VO> pageInfo, ${className}VO param) {

        return ${className?uncap_first}Mapper.findPage(pageInfo, param);
    }

    /**
     * 查询一条数据
     *
    <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
    </#list>
     * @return {@link ${className}VO}
     **/
    public ${className}VO getOne(<#list pkColumns as column>${column.attrType} ${column.attrName}<#if column_has_next>, </#if></#list>) {
        ${className}VO result = ${className?uncap_first}Mapper.findByPrimaryKey(<#list pkColumns as column>${column.attrName}<#if column_has_next>, </#if></#list>);

        return result;
    }

    /**
     * 新增数据
     *
     * @param data         数据
     * @return {@link Boolean} (false-失败，true-成功)
     **/
    public boolean add(${className}AddDTO data) {
        ${className}Entity param = new ${className}Entity();
        <#list columns as column>
            <#if (pkColumns?size == 1 && column.columnKey == "PRI")>
            <#elseif (column.attrName == "createBy" || column.attrName == "updateBy" || column.attrName == "createTime" || column.attrName == "updateTime" || column.attrName == "deleted")>
            <#else>
        param.set${column.upcaseAttrName}(data.get${column.upcaseAttrName}());
            </#if>
        </#list>

        return this.save(param);
    }

    /**
     * 批量新增数据
     *
     * @param data        数据
     * @return {@link Boolean} (false-失败，true-成功)
     **/
    public boolean addBatch(List${r'<'}${className}AddDTO> data) {
        List${r'<'}${className}Entity> params = new ArrayList<>();
        ${className}Entity param;
        for(${className}AddDTO item : data) {
            param = new ${className}Entity();
        <#list columns as column>
            <#if (pkColumns?size == 1 && column.columnKey == "PRI")>
            <#elseif (column.attrName == "createBy" || column.attrName == "updateBy" || column.attrName == "createTime" || column.attrName == "updateTime" || column.attrName == "deleted")>
            <#else>
            param.set${column.upcaseAttrName}(item.get${column.upcaseAttrName}());
            </#if>
        </#list>
            params.add(param);
        }

        return this.saveBatch(params);
    }

    /**
     * 更新数据
     *
     * @param data         数据
     * @return {@link Boolean} (false-失败，true-成功)
     **/
    public boolean modify(${className}UpdateDTO data) {
        <#list pkColumns as column>
        if (data.get${column.upcaseAttrName}() == null) {
            return false;
        }
        </#list>
        ${className}Entity param = new ${className}Entity();
        <#list columns as column>
            <#if (column.attrName == "createBy" || column.attrName == "updateBy" || column.attrName == "createTime" || column.attrName == "updateTime" || column.attrName == "deleted")>
            <#else>
        param.set${column.upcaseAttrName}(data.get${column.upcaseAttrName}());
            </#if>
        </#list>

        return this.updateById(param);
    }

    /**
     * 移除一条数据
     *
    <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
    </#list>
     * @return {@link Boolean} (false-失败，true-成功)
     **/
    public boolean removeOne(<#list pkColumns as column>${column.attrType} ${column.attrName}<#if column_has_next>, </#if></#list>) {
        <#if (pkColumns?size == 1)>
        int result = ${className?uncap_first}Mapper.deleteById(${pkColumns[0].attrName});
        <#else>
        int result = ${className?uncap_first}Mapper.delete(new LambdaQueryWrapper${r'<'}${className}Entity>()<#list pkColumns as column>.eq(${className}Entity::get${column.attrName?cap_first}, ${column.attrName})</#list>);
        </#if>

        return result > 0;
    }
}
