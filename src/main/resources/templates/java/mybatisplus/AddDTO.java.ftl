package ${basePackage}.dto;

<#if enableGenerateDoc>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveLocalDate>
import java.time.LocalDate;
</#if>
<#if haveLocalDateTime>
import java.time.LocalDateTime;
</#if>
<#if haveLocalTime>
import java.time.LocalTime;
</#if>
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

/**
 * ${comment}(新增时使用)
 *
 * @author ${author}
 * @since ${createTime}
 */
<#if enableGenerateDoc>
@ApiModel(value = "${comment}(新增时使用)")
</#if>
@Data
@Accessors(chain = true)
public class ${className}AddDTO implements Serializable {
    private static final long serialVersionUID = 1L;
<#if (pkColumns?size > 1)>
    <#list pkColumns as pkColumn>
    /**
     * ${pkColumn.columnComment}
     */
    @NotNull(message = "${pkColumn.attrName} can't be NULL")
    @ApiModelProperty(value = "${pkColumn.columnComment}<#if pkColumn.characterMaximumLength??> (maxLength: ${pkColumn.characterMaximumLength})</#if>", dataType = "${pkColumn.fullAttrType}", example = "${pkColumn.example}", required = true)
    private ${pkColumn.attrType} ${pkColumn.attrName};
    </#list>
</#if>
<#list columns as column>
    <#if (column.columnKey == "PRI" || column.attrName == "createBy" || column.attrName == "updateBy" || column.attrName == "createTime" || column.attrName == "updateTime" || column.attrName == "deleted")>
    <#else>
    /**
     * ${column.columnComment}
     */
        <#if (column.required)>
            <#if (column.attrType == "String" && column.columnDefault?? && column.columnDefault == "")>
            <#else>
    @NotNull(message = "${column.attrName} can't be NULL")
            </#if>
            <#if (column.attrType == "String")>
                <#if (column.attrName == "phone" || column.attrName == "mobile")>
    @Mobile
                <#elseif (column.attrName == "idCardNo")>
    @ChinaIDCardNo
                <#elseif (column.attrName == "email")>
    @CustomEmail
                <#else>
                </#if>
            </#if>
        </#if>
        <#if (column.attrType == "String" && column.characterMaximumLength??)>
    @Length(min = 0, max = ${column.characterMaximumLength}<#if column.characterMaximumLength??>, message = "max length: ${column.characterMaximumLength}"</#if>)
        </#if>
        <#if (column.attrType == "Integer" && (column.attrName == "sex" || column.attrName == "gender"))>
    @Range(min = 0, max = 2)
        </#if>
        <#if enableGenerateDoc>
            <#if (column.attrType == "String")>
    @ApiModelProperty(value = "${column.columnComment}<#if column.characterMaximumLength??> (maxLength: ${column.characterMaximumLength})</#if>", dataType = "${column.fullAttrType}", example = "${column.example}", required = <#if (column.required)>true<#else>false</#if>)
            <#else>
    @ApiModelProperty(value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "${column.example}", required = <#if (column.required)>true<#else>false</#if>)
            </#if>
        </#if>
    private ${column.attrType} ${column.attrName};
    </#if>
</#list>
}
