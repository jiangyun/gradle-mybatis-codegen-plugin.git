package io.github.jyrmc.gmcp.generate

import io.github.jyrmc.gmcp.common.FreemarkerConstant
import io.github.jyrmc.gmcp.entity.TableEntity
import io.github.jyrmc.gmcp.utils.PrintInfoUtil
import freemarker.template.Configuration
import freemarker.template.Template
import java.io.*
import java.nio.charset.StandardCharsets

/**
 *
 * @描述：生成vue2.x页面
 * @时间：2021/3/1 5:21 下午
 * @作者：jy
 * @公司：贵州中测信息技术有限公司
 */
object GenerateVuePage {

    /**
     * 生成vue页面
     */
    fun freemarkerGenerate(
        config: Configuration,
        projectPath: String,
        tableInfo: TableEntity,
        dataMap: MutableMap<String, Any?>
    ) {
        val fileName = "${tableInfo.className}Page.vue"
        val fileFullPath = "${projectPath}/src/main/resources/pages/${fileName}"

        val file = File(fileFullPath)
        if (file.exists()) {
            println("message: file [${fileName}] is exists.")
            return
        }
        if (!file.exists()) {
            file.parentFile.mkdirs()
            PrintInfoUtil.printCreateFold(file.parentFile.absolutePath)
        }

        val template: Template = config.getTemplate(
            "Page.vue.ftl",
            "UTF-8"
        )

        val out: Writer = BufferedWriter(OutputStreamWriter(FileOutputStream(fileFullPath), StandardCharsets.UTF_8))
        template.process(dataMap, out)
        out.flush()
        out.close()

        PrintInfoUtil.printCreateFile(fileFullPath)
    }
}