package io.github.jyrmc.gmcp.common

object TaskConstant {

    const val GEN_CODE_TASK_NAME = "generateCode"
    const val GEN_MARKDOWN_TASK_NAME = "generateMarkdown"
    const val DEFAULT_TASK_GROUP = "gmcp"
}