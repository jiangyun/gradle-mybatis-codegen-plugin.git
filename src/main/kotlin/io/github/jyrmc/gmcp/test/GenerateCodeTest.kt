package io.github.jyrmc.gmcp.test

import io.github.jyrmc.gmcp.enums.FileStrategyType
import io.github.jyrmc.gmcp.utils.GenerateCodeUtil

fun main() {
    GenerateCodeUtil.generateCode(
        dbType = "mysql",
        url = "jdbc:mysql://192.168.2.72:3306/uniplore-service-commons?useUnicode=true&serverTimezone=Asia/Shanghai&useSSL=false&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&autoReconnect=true&failOverReadOnly=false&connectionCollation=utf8mb4_general_ci",
        databaseName = "uniplore-service-commons",
        userName = "root",
        password = "Uniplore!@#123",
//        dbType = "postgresql",
//        url = "jdbc:postgresql://192.168.2.140:32417/uniplore-micro-basic",
//        databaseName = "postgres",
//        userName = "postgres",
//        password = "postgres",
//        frameworkType = "mybatisplus",
        frameworkType = "openDoc",
        tablePrefix = "",
        tableNames = arrayListOf("uniplore_sys_product"),
//        ignoreColumns = arrayListOf("tenant_id", "sys_group_id"),
        ignoreColumns = arrayListOf(),
        projectPath = "C:\\Users\\jy\\Desktop\\CodeGen",
        packageName = "cn.uniplore.core.quartz",
        controller = "controller",
        service = "service",
        mapper = "mapper",
        entity = "entity",
        xml = "mapper",
        languageType = "java",
        author = "jy",
        apiPrefix = "",
        restfulStyle = true,
        swagger = true,
        fileGenStrategy = FileStrategyType.CREATE_NEW,
    )
}