package io.github.jyrmc.gmcp.datatype

/**
 * 创建于 2020-05-26 22:59
 *
 * @author jiangyun
 * @类说明：pgsql数据类型
 */
object PostgresqlDataType {
    /**
     * 获取postgresql字段类型信息
     *
     * @param languageType 语言类型(java,kotlin)
     * @return
     */
    fun getPostgresqlParamType(languageType: String): MutableMap<String, String> {
        val type: MutableMap<String, String> = mutableMapOf()
        type["bigint"] = "Long"
        type["int8"] = "Long"
        type["bigserial"] = "Long"
        type["serial8"] = "Long"
        type["bit"] = "Boolean"
        type["varbit"] = "Boolean"
        type["boolean"] = "Boolean"
        type["bool"] = "Boolean"
        type["box"] = if (languageType == "java") "Object" else "Any"
        //type["bytea"] = if (languageType == "java") "byte[]" else "Any"
        type["varchar"] = "String"
        type["char"] = "String"
        type["bpchar"] = "String"
        type["cidr"] = "String"
        type["circle"] = if (languageType == "java") "Object" else "Any"
        type["date"] = "Date"
        type["double precision"] = "Double"
        type["float8"] = "Double"
        type["inet"] = "String"
        type["integer"] = if (languageType == "java") "Integer" else "Int"
        type["int"] = if (languageType == "java") "Integer" else "Int"
        type["int4"] = if (languageType == "java") "Integer" else "Int"
        type["interval"] = if (languageType == "java") "Object" else "Any"
        type["line"] = if (languageType == "java") "Object" else "Any"
        type["lseg"] = if (languageType == "java") "Object" else "Any"
        type["path"] = if (languageType == "java") "Object" else "Any"
        type["point"] = if (languageType == "java") "Object" else "Any"
        type["polygon"] = if (languageType == "java") "Object" else "Any"
        type["macaddr"] = "String"
        type["money"] = "BigDecimal"
        type["numeric"] = "BigDecimal"
        type["decimal"] = "BigDecimal"
        type["real"] = "Float"
        type["float4"] = "Float"
        type["smallint"] = if (languageType == "java") "Integer" else "Int"
        type["int2"] = if (languageType == "java") "Integer" else "Int"
        type["serial"] = if (languageType == "java") "Integer" else "Int"
        type["serial4"] = if (languageType == "java") "Integer" else "Int"
        type["text"] = "String"
        type["time"] = "LocalDateTime"
        type["timetz"] = "LocalDateTime"
        type["timestamp"] = "LocalDateTime"
        type["timestamptz"] = "LocalDateTime"

        return type
    }

    /**
     * 获取postgresql完整字段类型信息
     *
     * @param languageType 语言类型(java,kotlin)
     * @return
     */
    fun getFullPostgresqlParamType(languageType: String): MutableMap<String, String> {
        val fullType: MutableMap<String, String> = mutableMapOf()
        fullType["bigint"] = if (languageType == "java") "java.lang.Long" else "Long"
        fullType["int8"] = if (languageType == "java") "java.lang.Long" else "Long"
        fullType["bigserial"] = if (languageType == "java") "java.lang.Long" else "Long"
        fullType["serial8"] = if (languageType == "java") "java.lang.Long" else "Long"
        fullType["bit"] = if (languageType == "java") "java.lang.Long" else "Boolean"
        fullType["varbit"] = if (languageType == "java") "java.lang.Long" else "Boolean"
        fullType["boolean"] = if (languageType == "java") "java.lang.Long" else "Boolean"
        fullType["bool"] = if (languageType == "java") "java.lang.Long" else "Boolean"
        fullType["box"] = if (languageType == "java") "java.lang.Object" else "Any"
        //fullType["bytea"] = if (languageType == "java") "byte[]" else "Any"
        fullType["varchar"] = if (languageType == "java") "java.lang.String" else "String"
        fullType["char"] = if (languageType == "java") "java.lang.String" else "String"
        fullType["bpchar"] = if (languageType == "java") "java.lang.String" else "String"
        fullType["cidr"] = if (languageType == "java") "java.lang.String" else "String"
        fullType["circle"] = if (languageType == "java") "java.lang.Object" else "Any"
        fullType["date"] = if (languageType == "java") "java.util.Date" else "Date"
        fullType["double precision"] = if (languageType == "java") "java.lang.Double" else "Double"
        fullType["float8"] = if (languageType == "java") "java.lang.Double" else "Double"
        fullType["inet"] = if (languageType == "java") "java.lang.String" else "String"
        fullType["integer"] = if (languageType == "java") "java.lang.Integer" else "Int"
        fullType["int"] = if (languageType == "java") "java.lang.Integer" else "Int"
        fullType["int4"] = if (languageType == "java") "java.lang.Integer" else "Int"
        fullType["interval"] = if (languageType == "java") "java.lang.Object" else "Any"
        fullType["line"] = if (languageType == "java") "java.lang.Object" else "Any"
        fullType["lseg"] = if (languageType == "java") "java.lang.Object" else "Any"
        fullType["path"] = if (languageType == "java") "java.lang.Object" else "Any"
        fullType["point"] = if (languageType == "java") "java.lang.Object" else "Any"
        fullType["polygon"] = if (languageType == "java") "java.lang.Object" else "Any"
        fullType["macaddr"] = if (languageType == "java") "java.lang.String" else "String"
        fullType["money"] = "java.math.BigDecimal"
        fullType["numeric"] = "java.math.BigDecimal"
        fullType["decimal"] = "java.math.BigDecimal"
        fullType["real"] = if (languageType == "java") "java.lang.Float" else "Float"
        fullType["float4"] = if (languageType == "java") "java.lang.Float" else "Float"
        fullType["smallint"] = if (languageType == "java") "java.lang.Integer" else "Int"
        fullType["int2"] = if (languageType == "java") "java.lang.Integer" else "Int"
        fullType["serial"] = if (languageType == "java") "java.lang.Integer" else "Int"
        fullType["serial4"] = if (languageType == "java") "java.lang.Integer" else "Int"
        fullType["text"] = if (languageType == "java") "java.lang.String" else "String"
        fullType["time"] = if (languageType == "java") "java.lang.Long" else "Long"
        fullType["timetz"] = if (languageType == "java") "java.lang.Long" else "Long"
        fullType["timestamp"] = if (languageType == "java") "java.lang.Long" else "Long"
        fullType["timestamptz"] = if (languageType == "java") "java.lang.Long" else "Long"

        return fullType
    }
}