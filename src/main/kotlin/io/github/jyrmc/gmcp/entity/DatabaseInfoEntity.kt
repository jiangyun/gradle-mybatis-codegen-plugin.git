package io.github.jyrmc.gmcp.entity

/**
 * 创建于 2020-01-08 16:07
 *
 * @author jiangyun
 * @类说明：数据库连接信息
 */
data class DatabaseInfoEntity(
    var url: String,        // 数据库地址
    var userName: String,   // 用户名
    var password: String    // 密码
)