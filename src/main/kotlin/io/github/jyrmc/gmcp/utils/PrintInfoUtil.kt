package io.github.jyrmc.gmcp.utils

/**
 * 创建于 2020-05-21 16:02
 *
 * @author jiangyun
 * @类说明：控制台打印输出语句
 */
object PrintInfoUtil {

    /**
     * 创建文件夹时打印语句格式控制
     *
     * @param path 路径
     */
    fun printCreateFold(path: String) {
        println("Create Folder：[${path}]")
    }

    /**
     * 创建文件打印控制
     *
     * @param pathAndName 路径和文件名
     */
    fun printCreateFile(pathAndName: String) {
        println("Create File：${pathAndName}")
    }
}