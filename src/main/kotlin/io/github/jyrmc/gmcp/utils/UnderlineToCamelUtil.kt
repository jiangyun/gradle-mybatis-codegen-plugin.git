package io.github.jyrmc.gmcp.utils

import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * 创建于 2020-01-08 22:23
 *
 * @author jiangyun
 * @类说明：下划线驼峰互换
 */
object UnderlineToCamelUtil {
    /**
     * 下划线转驼峰法
     *
     * @param line          源字符串
     * @param smallCamel    大小驼峰,是否为小驼峰
     * @return 转换后的字符串
     */
    fun underlineToCamel(data: String, smallCamel: Boolean): String {
        return if (data.isBlank()) {
            ""
        } else {
            val sb = StringBuffer()

            val pattern: Pattern = Pattern.compile("([A-Za-z\\d]+)(_)?")
            val matcher: Matcher = pattern.matcher(data)
            var word: String?
            while (matcher.find()) {
                word = matcher.group()
                if (smallCamel && matcher.start() == 0) {
                    sb.append(word[0].lowercaseChar())
                } else {
                    sb.append(word[0].uppercaseChar())
                }
                val index: Int = word.lastIndexOf('_')
                if (index > 0) {
                    sb.append(word.substring(1, index).lowercase(Locale.getDefault()))
                } else {
                    sb.append(word.substring(1).lowercase(Locale.getDefault()))
                }
            }

            sb.toString()
        }
    }

    /**
     * 下划线转驼峰法
     *
     * @param line          源字符串
     * @param smallCamel    大小驼峰,是否为小驼峰
     * @return 转换后的字符串
     */
    fun underlineToCamel(data: String, prefix: String): String {
        return if (data.isBlank()) {
            ""
        } else {
            val sb = StringBuffer()

            val pattern: Pattern = Pattern.compile("([A-Za-z\\d]+)(_)?")
            val matcher: Matcher = pattern.matcher(data)
            var word: String?
            while (matcher.find()) {
                word = matcher.group()
                if (matcher.start() == 0) {
                    sb.append(word[0].lowercaseChar())
                } else {
                    sb.append(word[0].uppercaseChar())
                }
                val index: Int = word.lastIndexOf('_')
                if (index > 0) {
                    sb.append(word.substring(1, index).lowercase(Locale.getDefault()))
                } else {
                    sb.append(word.substring(1).lowercase(Locale.getDefault()))
                }
            }


            sb.toString().replace(prefix, "")
        }
    }

    /**
     * 驼峰法转下划线
     *
     * @param data        源字符串
     * @param toLowCase   是否为小写
     * @return 转换后的字符串
     */
    fun camelToUnderline(data: String, toLowCase: Boolean): String? {
        return if (data.isBlank()) {
            ""
        } else {
            val tempData: String = data[0].uppercaseChar() + data.substring(1)
            val sb = StringBuffer()
            val pattern: Pattern = Pattern.compile("[A-Z]([a-z\\d]+)?")
            val matcher: Matcher = pattern.matcher(tempData)
            var word: String?
            while (matcher.find()) {
                word = matcher.group()
                sb.append(if (toLowCase) word.lowercase(Locale.getDefault()) else word.uppercase(Locale.getDefault()))
                if (matcher.end() == tempData.length) {
                    sb.append("")
                } else {
                    sb.append("_")
                }
            }

            sb.toString()
        }
    }

    /**
     * 首字母转大写或小写
     *
     * @param data        源字符串
     * @param toLowCase   是否为小写
     * @return 转换后的字符串
     */
    fun firstCharToUpcaseOrLowcase(data: String, toLowCase: Boolean): String {
        return if (data.isBlank()) {
            ""
        } else {
            val sb = StringBuffer()
            if (!toLowCase) {
                sb.append(data[0].uppercaseChar())
            } else {
                sb.append(data[0].lowercaseChar())
            }
            sb.append(data.substring(1, data.length))

            sb.toString()
        }
    }
}