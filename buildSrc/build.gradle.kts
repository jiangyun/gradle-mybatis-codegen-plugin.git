plugins {
    `kotlin-dsl`
}

repositories {
    mavenLocal()
    mavenCentral()
    gradlePluginPortal()
}


dependencies {
    testImplementation(kotlin("test"))
}
