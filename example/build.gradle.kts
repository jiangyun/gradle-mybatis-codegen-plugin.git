import io.github.jyrmc.gmcp.plugin.GradleMybatisCodegenPluginExtension

buildscript {
    repositories {
        mavenLocal()
        mavenCenter()
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
    }

    dependencies {
        // classpath("io.github.jyrmc:gradle-mybatis-codegen-plugin:1.0.2")
    }
}

plugins {
    kotlin("jvm") version "1.8.0"
    id("io.github.jyrmc") version "1.0.2"
}

//apply {
//	plugin("io.github.jyrmc")
//}

configure<io.github.jyrmc.gmcp.extension.MybatisCodegenExtension> {
    dbType.set("mysql")
    url.set("localhost")
    port.set(3306)
    database.set("uniplore-micro-basic")
    userName.set("root")
    password.set("admin123456")
    frameworkType.set("mybatisplus")
    tablePrefix.set("uniplore")
    tables.set(arrayListOf("uniplore_sys_user", "uniplore_sys_user_role"))
    projectPath.set(project.path)
    packageName.set("com.example.gmcp")
    controller.set("controller")
    service.set("service")
    mapper.set("mapper")
    entity.set("entity")
    xml.set("mapper")
    languageType.set("java")
    author.set("jyRMc")
    apiPrefix.set("api")
    restfulStyle.set(true)
    swagger.set(true)
}

repositories {
    mavenLocal()
    mavenCentral()
}

