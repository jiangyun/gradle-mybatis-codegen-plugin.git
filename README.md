<p align="center">
    <img src="docs/img/logo.svg" width="260">
</p>
<p align="center">
  <img src="https://img.shields.io/badge/Gradle-7.6-blue" alt="Gradle"/>
  <img src="https://img.shields.io/badge/Kotlin-latest-blueviolet" alt="Kotlin"/>
  <img src="https://img.shields.io/badge/Freemaker-2.3.31-brightgreen" alt="Freemaker"/>
</p>

# 基于gradle mybatis-plus的代码生成插件

**注：resources/template下的引擎模板代码不能格式化，切记，否则会导致生成的代码格式特别乱！！！**

## 介绍
基于gradle构建工具并支持gradle的代码生成插件，生成代码支持类型如下：

* 数据库类型
    * [x] mysql5.6+
    * [x] postgresql12+
    * [x] mariadb10+
    * [ ] h2
    * [ ] sqlite
    * [ ] mssql
    * [ ] oracle
    * [ ] db2
    * [ ] tidb
    * [ ] sybase

* 支持框架及特点
    * [x] gradle
    * [x] maven
    * [x] springboot
    * [x] springmvc
    * [x] mybatis
    * [x] mybatis-plus
    * [x] swagger2
    * [x] swagger-bootstrap-ui
    * [x] knife4j

* 支持语言
    * [x] java
    * [x] kotlin

> 生成的代码，在mybatis版本中，是默认直接生成从dao层到controller层的CRUD，mybatis-plus则按照官方代码生成方式
> 目前暂时推荐使用创建独立的gradle文件，并在build.gradle或build.gradle.kts中引用即可。


## 软件架构
1. kotlin;
2. freemarker；
3. gradle；
4. druid;

## 发布插件
```bash
./gradlew publishPlugins
# 如果已经安装gradle7+
gradle publishPlugins
```

## 使用

#### groovy
Using the plugins DSL:
```groovy
plugins {
  id("io.github.jyrmc") version "2.0.2"
}
```

Using legacy plugin application:

```groovy
buildscript {
  repositories {
    maven {
      url = uri("https://plugins.gradle.org/m2/")
    }
  }
  dependencies {
    classpath("io.github.jyrmc:gradle-mybatis-codegen-plugin:2.0.2")
  }
}
```

#### kotlin

Using the plugins DSL:
```groovy
plugins {
  id("io.github.jyrmc") version "2.0.2"
}
```

Using legacy plugin application:
```groovy
buildscript {
  repositories {
    maven {
      url = uri("https://plugins.gradle.org/m2/")
    }
  }
  dependencies {
    classpath("io.github.jyrmc:gradle-mybatis-codegen-plugin:2.0.2")
  }
}

apply(plugin = "io.github.jyrmc")

configure<io.github.jyrmc.gmcp.extension.MybatisCodegenExtension> {
  dbType.set("mysql") // 数据库类型：mysql(mariadb)/postgresql
  url.set("localhost") // 数据库地址
  port.set(3306) // 数据库端口
  database.set("jyRMc_test_database") // 数据库名称
  userName.set("root") // 用户名
  password.set("admin123456") // 密码
  frameworkType.set("mybatisplus") // 生成代码类型： mybatis/mybatisplus， 建议mybatisplus
  tablePrefix.set("iwiteks") // 设置需要忽略的表前缀，如表名为iwiteks_user，则设置后生成的Entity java文件为：UserEntity，否则为IwiteksUserEntity
  tables.set(arrayListOf( // 设置需要生成代码的表
          "jyrmc_sys_user",
          "jyrmc_sys_user_role",
  ))
  ignoreColumns.set(arrayListOf( // 排除需要生成代码的字段(列)
          "tenant_id",
          "sys_group_id",
  ))
  projectPath.set("/Users/jiangyun/IdeaProjects/gmcp-demo") // 项目所在绝对地址
  packageName.set("com.example.gmcp") // 包名称
  controller.set("controller") // 控制器包名称
  service.set("service") // 业务逻辑层包名称
  mapper.set("mapper") // mapper层包名称
  entity.set("entity") // 实体包名称
  xml.set("mapper") // 项目resources文件夹下mapper对应的xml所在文件夹名称
  languageType.set("java") // 生成语言类型：java/kotlin, kotlin未经过严格测试
  author.set("jyRMc") // 生成代码作者
  apiPrefix.set("api") // controller路由前缀
  restfulStyle.set(true) // 是否为restful分隔
  swagger.set(true) // 是否支持生产swagger注解，便于生成swagger文档
}
```

## 集成后idea中的效果
![task.png](snapshot/task.png)

4. 运行代码即可生成我们需要的代码。


## 代码生成中引用源码

| 源码文件名            | 源码文档                                             | 文档说明      |
|:-----------------|:-------------------------------------------------|:----------|
| IResultCode.java | [IResultCode.java文档地址](docs/code/IResultCode.md) | 返回码接口定义   |
| ResultCode.java | [ResultCode.java文档地址](docs/code/ResultCode.md)        | 返回码接口枚举实现 |
| Result.java | [Result.java文档地址](docs/code/Result.md)            | 统一响应消息 |

## 代码效果生成截图

#### controller
  ![controller.png](snapshot/controller.png)
#### service
  ![service.png](snapshot/service.png)
#### mapper
  ![mapper.png](snapshot/mapper.png)
#### mapperxml
  ![mapperxml.png](snapshot/mapperxml.png)
#### entity
  ![entity.png](snapshot/entity.png)
#### vo
  ![vo.png](snapshot/vo.png)
#### dto
  ![adddto.png](snapshot/adddto.png)
  ![updatedto.png](snapshot/updatedto.png)
