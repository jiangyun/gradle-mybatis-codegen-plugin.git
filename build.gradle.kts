import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    repositories {
        mavenCentral()
        maven { setUrl("https://maven.aliyun.com/repository/central") }
        maven { setUrl("https://maven.aliyun.com/repository/public") }
        maven { setUrl("https://maven.aliyun.com/repository/google") }
        maven { setUrl("https://maven.aliyun.com/repository/gradle-plugin") }
        maven { setUrl("https://maven.aliyun.com/repository/spring") }
        maven { setUrl("https://maven.aliyun.com/repository/spring-plugin") }
        gradlePluginPortal()
    }
}

plugins {
    `java-gradle-plugin`
    kotlin("jvm") version "1.8.0"
    id("maven-publish")
//    id("signing")
    id("com.gradle.plugin-publish") version Versions.pluginPublishingVersion
}

group = "io.github.jyrmc"
version = "2.0.4"

repositories {
    mavenCentral()
    maven { setUrl("https://maven.aliyun.com/repository/central") }
    maven { setUrl("https://maven.aliyun.com/repository/public") }
    maven { setUrl("https://maven.aliyun.com/repository/google") }
    maven { setUrl("https://maven.aliyun.com/repository/gradle-plugin") }
    maven { setUrl("https://maven.aliyun.com/repository/spring") }
    maven { setUrl("https://maven.aliyun.com/repository/spring-plugin") }
}

dependencies {
    implementation("org.freemarker:freemarker:${Versions.freemarkerVersion}")
    implementation("com.alibaba:druid:${Versions.druidVersion}")
    implementation("mysql:mysql-connector-java:${Versions.mysqlDriverVersion}")
    implementation("org.postgresql:postgresql:${Versions.pgsqlDriverVersion}")
    implementation("com.microsoft.sqlserver:mssql-jdbc:${Versions.mssqlDriverVersion}")
    implementation("com.oracle.database.jdbc:ojdbc8:${Versions.oracleDriverVersion}")
    implementation(gradleApi())
    testImplementation(kotlin("test"))
}

gradlePlugin {
    website.set("https://gitee.com/jiangyun/gradle-mybatis-codegen-plugin")
    vcsUrl.set("https://gitee.com/jiangyun/gradle-mybatis-codegen-plugin.git")
    plugins {
        create("gradleMybatisCodegenPlugin") {
            id = "io.github.jyrmc"
            displayName = "Generate MyaBtis Code Gradle plugins"
            description = "Plugin for generate java code & markdown doc of Gradle plugins"
            tags.set(listOf("mybatis", "mysql", "postgresql", "markdown"))
            implementationClass = "io.github.jyrmc.gmcp.plugin.GradleMybatisCodegenPlugin"
        }
    }
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}

publishing {
    publications {
        create<MavenPublication>("MyTest") {
            groupId = project.group.toString()
            artifactId = project.name
            version = project.version.toString()
            description = project.description
            from(components["java"])

            pom {
                developers {
                    developer {
                        id.set("io.github.jyrmc")
                        name.set("jyRMc")
                        email.set("972516490@qq.com")
                    }
                }
            }
        }
        repositories {
            mavenLocal()
        }
    }
    // 远程发布私有库
    publications {
        create<MavenPublication>("Uniplore") {
            groupId = project.group.toString()
            artifactId = project.name
            version = project.version.toString()
            description = project.description
            from(components["java"])

            pom {
                developers {
                    developer {
                        id.set("io.github.jyrmc.ssh")
                        name.set("jyRMc")
                        email.set("972516490@qq.com")
                    }
                }
            }
        }
        repositories {
            maven {
                isAllowInsecureProtocol = true
                setUrl("http://192.168.2.104:8081/repository/maven-releases/")
                credentials {
                    username = "admin"
                    password = "Uniplore@123456"
                }
            }
        }
    }
}


