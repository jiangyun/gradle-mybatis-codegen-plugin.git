# 返回码接口(IResultCode)定义源码

```java
package com.iwiteks.core.common.web;

/**
 * 返回码接口
 *
 * @author jy
 * @since 2022/4/24 14:46
 **/
public interface IResultCode {

	/**
	 * 返回码
	 *
	 * @return int
	 */
	int getCode();

	/**
	 * 返回消息
	 *
	 * @return String
	 */
	String getMsg();
}

```